# Desafio Avanti Bootcamp Devops

## Sobre o projeto

Este projeto tem como objetivo por em prática conceitos e conhecimentos adiquiridos durante as prineiras aulas do Avanti bootcamp DevOps, utilizando Terraform e AWS.

## Passo a Passo

### Terraform Plan

O comando terraform plan' é usado para visualizar as alterações planejadas na infraestrutura com base nos arquivos de configuração. Ele mostra o que o Terraform vai adicionar, modificar ou remover, sem realizar efetivamente nenhuma mudança. Isso permite revisar e validar as alterações propostas antes de aplicá-las.

<img src="assets/images/plan.png" width="720" height="420">

### Terraform apply

O comando 'terraform apply' é usado para aplicar as alterações planejadas na infraestrutura definida nos arquivos de configuração do Terraform. Ele executa a criação, modificação ou remoção de recursos conforme especificado, atualizando o estado da infraestrutura no arquivo 'terraform.tfstate'. É importante revisar as alterações planejadas com 'terraform plan' antes de executar 'terraform apply' para garantir que as mudanças sejam aplicadas corretamente.

<img src="assets/images/apply.png" width="720" height="420">

### Instância EC2 em execução
<img src="assets/images/ec2.png" width="720" height="420">

### Registrando runner na instância EC2
<img src="assets/images/runner.png" width="720" height="420">

### Pipelines
<img src="assets/images/pipelines.png" width="720" height="420">

### Imagem do site
<img src="assets/images/site.png" width="720" height="420">