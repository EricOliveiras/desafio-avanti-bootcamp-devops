#!/bin/bash
sudo apt update
sudo apt upgrade -y

sudo apt-get install apache2 -y

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner
sudo chown gitlab-runner /var/www/html/ -R
